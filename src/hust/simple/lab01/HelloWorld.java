package hust.simple.lab01;

import java.util.Calendar;
import java.util.Scanner;

public class HelloWorld
{
	public static void main(String[] argc)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Hello. What's your name:");
		String name = scan.nextLine();
		System.out.println("Enter your birth year:");
		int year = Number.inputInt();
		int currentyear = Calendar.getInstance().get(Calendar.YEAR);
		int age = Calculation.minus(currentyear, year);
		System.out.println("Hello " + name);
		System.out.println("Your age is " + age);
	}
}
