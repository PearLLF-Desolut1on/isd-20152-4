package hust.simple.lab01;

public class FirstNumberApp
{
	public static void main(String[] args)
	{
		int a;
		System.out.print("Input an integer: ");
		a = Number.inputInt();

		if (!Number.isPrime(a))
			System.out.printf("%d is NOT a PRIME number\n", a);
		else
			System.out.printf("%d is a PRIME number\n", a);

		if (!Number.isSquare(a))
			System.out.printf("%d is NOT a SQUARE number\n", a);
		else
			System.out.printf("%d is a SQUARE number\n", a);

		if (!Number.isPerfect(a))
			System.out.printf("%d is NOT a PERFECT number\n", a);
		else
			System.out.printf("%d is a PERFECT number\n", a);
	}
}
