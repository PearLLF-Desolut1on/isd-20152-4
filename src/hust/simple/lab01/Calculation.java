package hust.simple.lab01;

public class Calculation
{
	/**
	 * Add b to a.
	 * @param a
	 * @param b
	 * @return a + b
	 */
	public static int plus(int a, int b)
	{
		return a + b;
	}

	/**
	 * Subtract a by b.
	 * @param a 
	 * @param b
	 * @return a - b
	 */
	public static int minus(int a, int b)
	{
		return a - b;
	}

	/**
	 * Multiply 2 integers.
	 * @param a first factor
	 * @param b second factor
	 * @return product
	 */
	public static int times(int a, int b)
	{
		return a * b;
	}

	/**
	 * Divides 2 integers.
	 * @param a dividend
	 * @param b divisor
	 * @return quotient
	 */
	public static double divides(int a, int b)
	{
		double quotient = 0;

		try
		{
			quotient = a / b;
		}

		catch (ArithmeticException arithmeticException)
		{
			System.err.printf("Exception: %s\n", arithmeticException);
			System.out.println("Divisor cannot be zero.");
			System.exit(0);
		}
		
		return quotient;
	}
}