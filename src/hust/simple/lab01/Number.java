package hust.simple.lab01;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Number
{

	private static Scanner _scanner = new Scanner(System.in);

	/**
	 * Return an integer is read from the keyboard.
	 */
	public static int inputInt()
	{
		int input = 0;
		boolean success = false;
		
		do
		{
			try
			{
				input = _scanner.nextInt();
				success = true;
				_scanner.close();
			} 
			
			catch (InputMismatchException inputMismatchException)
			{
				System.err.printf("Exception: %s\n", inputMismatchException);
				System.out.println("You must enter integers. Please try again.");
				_scanner.nextLine();
			}
		} while (!success);
	
		return input;
	}

	/**
	 * Check if an integer is prime number.
	 */
	public static boolean isPrime(int i)
	{
		if (i < 2)
			return false;
		else if (i == 2)
			return true;

		for (int d = 2; d <= Math.sqrt(i); d++)
		{
			if (i % d == 0)
				return false;
		}
		
		return true;
	}

	/**
	 * Check if an integer is square number.
	 */
	public static boolean isSquare(int i)
	{
		double sqrt = Math.sqrt(i);
		return (sqrt - (int) sqrt) == 0;
	}
	
	/**
	 * Check if an integer is perfect number.
	 */
	public static boolean isPerfect(int i)
	{
		int tmp = 1;
		for (int j = 2; j < Math.sqrt(i); j++)
		{
			if(i % j == 0)
			{
				tmp += j + i / j;
			}
		}
		
		if(tmp == i)
			return true;
		
		return false;
	}
}
